#!/bin/sh

docker-compose -f docker-compose-master.yml up -d

until token=$(docker exec -t -i k0s k0s token create --role=worker); do
  >&2 echo "k0s is unavailable - sleeping"
  sleep 1
done

echo "K0S_TOKEN=$token" > .env

docker exec k0s cat /var/lib/k0s/pki/admin.conf > k0s-kubeconfig.yml

docker-compose -f docker-compose-master.yml -f docker-compose-worker.yml up -d
