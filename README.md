# k0s_examples

Sample project to launch a k0s cluster within Docker (kind).  THIS IS NOT PROD PROOF and should not be taken as is !!! The loadbalancing service is provided by Traefik. As no mechanism to provide a public IP to the traefik service is provided here, to test it on must either use nodeports mechanism or as for the lazy me use kubectl portforward to expose the service to the world.

It also provides some deployement examples (hello-world, whoami, squash-orchestrator) with of course the associated services and ingresses. Some mechanisme for pod horizontal sacalability are also displayed.

## How To

The "bootsrap.sh" script will setup k0s and

1) Launch the master node usinf

   ``` bash
    docker-compose -f docker-compose-master.yml up -d
    ```

2) Copy the result of:

    ``` bash
    docker exec -t -i k0s k0s token create --role=worker
    ```

   right after "K0S_TOKEN=" in the [.env file](.env).

3) Get the kubeconfig file:

    ``` bash
    docker exec k0s cat /var/lib/k0s/pki/admin.conf > k0s-kubeconfig.yml
    ```

4) Launch the worker-node

   ``` bash
   docker-compose -f docker-compose-master.yml -f docker-compose-worker.yml up -d
    ```

Once this is done , one can

1) Check that everything is up and running

    ``` bash
    kubectl --kubeconfig k0s-kubeconfig.yml get nodes
    ```

    ``` bash
    kubectl --kubeconfig k0s-kubeconfig.yml get all
    ```

2) Deploy using kubectl/helm your favorite apps, ex:

    ``` bash
    kubectl --kubeconfig k0s-kubeconfig.yml apply -f examples/whoami.yml
    ```

Finally "cleanup.sh" has become death, the destroyer of the worlds. Basically it shuts everything down and removes any monted volumes. 
## Provided examples

- Hello-world : /hello-world will print in your browser "Hello World, Kubernetes"

- Whoami: /whoami wil print your infos in your browser

- Squash-orchestrator: Deploy a fully configure Squash-orchestrator in its opensource version with all the configured routes. Ii assumes that the public trusted keys used to verify the JWT Tokens in the secret "squash-orchestrator-trusted-keys". This secret **MUST** be created prior to applying the deployement descriptor. This secret must be holding the public keys used to verify the signed JWT tokens. below is a quick **How to** generate this secret

  1) Generate (or reuse) (at least) a pair of rsa keys

      ``` bash
      openssl genrsa -out pki/trusted_key.pem 4096
      openssl rsa -pubout -in pki/trusted_key.pem -out pki/trusted_key.pub
      ```

  2) Create the secret

      ``` bash
      kubectl --kubeconfig k0s-kubeconfig.yml create secret generic squash-orchestrator-trusted-keys --from-file=./pki/trusted_key.pub --namespace default
      ```

    If one does not want to use a specif pair of keys, one just have to comment out the relevant sections in [squash-orchestrator.yml](examples/squash-orchestrator.yml).
